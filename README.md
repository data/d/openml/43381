# OpenML dataset: Coursera-Course-Dataset

https://www.openml.org/d/43381

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is a dataset i generated during a hackathon for project purpose. Here i have scrapped data from Coursera official web site.  Our project aims to help any new learner get the right course to learn by just answering a few questions. It is an intelligent course recommendation system. Hence we had to scrap data from few educational websites. This is data scrapped from Coursera website. For the project visit: https://github.com/Siddharth1698/Coursu . Please do show your support by following us. I have just started to learn on data science and hope this dataset will be helpful to someone for his/her personal purposes. The scrapping code is here : https://github.com/Siddharth1698/Coursera-Course-Dataset
Article about the dataset generation : https://medium.com/analytics-vidhya/web-scraping-and-coursera-8db6af45d83f  

Content
This dataset contains mainly 6 columns and 890 course data. The detailed description:

course_title : Contains the course title.
course_organization : It tells which organization is conducting the courses.
courseCertificatetype : It has details about what are the different certifications available in courses.
course_rating : It has the ratings associated with each course.
course_difficulty : It tells about how difficult or what is the level of the course.
coursestudentsenrolled : It has the number of students that are enrolled in the course.

Inspiration
This is just one of my first scraped dataset. Follow my GitHub for more: https://github.com/Siddharth1698

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43381) of an [OpenML dataset](https://www.openml.org/d/43381). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43381/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43381/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43381/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

